import {EventEmitter, Injectable, Output} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {TokenService} from "./token.service";
import {Observable} from "rxjs";
import {LoginDto} from "../../interfaces/dtos/auth/LoginDto";
import {KeycloakTokenDto} from "../../interfaces/dtos/auth/KeycloakTokenDto";
import {RefreshDto} from "../../interfaces/dtos/auth/RefreshDto";
import {environment} from "../../../environments/environment";
import {Router} from "@angular/router";
import {RegisterDto} from "../../interfaces/dtos/auth/RegisterDto";

const HTTP_OPTIONS = {
  headers: new HttpHeaders({
    "Content-Type": "application/x-www-form-urlencoded"
  })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  @Output() isLoggedIn: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private httpClient: HttpClient, private tokenService: TokenService, private router: Router) {
  }

  getIsLoggedIn(): Observable<boolean> {
    this.isLoggedIn.next(this.tokenService.getAccessToken() != null)
    return this.isLoggedIn;
  }

  login(loginData: LoginDto): Observable<KeycloakTokenDto> {
    this.tokenService.removeAccessToken();
    this.tokenService.removeRefreshToken();

    const body = new HttpParams()
      .set("client_id", environment.AUTH_CLIENT_ID)
      .set("username", loginData.username)
      .set("password", loginData.password)
      .set("grant_type", environment.AUTH_LOGIN_GRANT_TYPE)

    const keycloakResponseObservable = this.httpClient.post<KeycloakTokenDto>(`${environment.AUTH_API_URL}/protocol/openid-connect/token`, body, HTTP_OPTIONS);
    keycloakResponseObservable.subscribe(response => {
      this.tokenService.saveAccessToken(response.access_token);
      this.tokenService.saveRefreshToken(response.refresh_token);
      this.isLoggedIn.next(true);
      // href forces page reload to avoid routerLink bug
      window.location.href = "/home";
    });

    return keycloakResponseObservable;
  }

  refreshToken(refreshData: RefreshDto): Observable<KeycloakTokenDto> {
    this.tokenService.removeAccessToken();
    this.tokenService.removeRefreshToken();

    const body = new HttpParams()
      .set("refresh_token", refreshData.refresh_token)
      .set("grant_type", environment.AUTH_REFRESH_GRANT_TYPE);

    const keycloakResponseObservable = this.httpClient.post<KeycloakTokenDto>(`${environment.AUTH_API_URL}/protocol/openid-connect/token`, body, HTTP_OPTIONS);
    keycloakResponseObservable.subscribe(response => {
      this.tokenService.saveAccessToken(response.access_token);
      this.tokenService.saveRefreshToken(response.refresh_token);
    });

    return keycloakResponseObservable;
  }

  logout(): Observable<KeycloakTokenDto> {
    this.tokenService.removeAccessToken();
    let refreshToken = this.tokenService.getRefreshToken();
    if (refreshToken == null) {
      refreshToken = "";
    }
    this.tokenService.removeRefreshToken();

    const body = new HttpParams()
      .set("client_id", environment.AUTH_CLIENT_ID)
      .set("refresh_token", refreshToken);

    const keycloakResponseObservable = this.httpClient.post<KeycloakTokenDto>(`${environment.AUTH_API_URL}/protocol/openid-connect/logout`, body, HTTP_OPTIONS);
    keycloakResponseObservable.subscribe(_ => {
      this.isLoggedIn.next(false);
    });
    return keycloakResponseObservable;
  }

  register(registerData: RegisterDto): Observable<RegisterDto> {

    const keycloakResponseObservable = this.httpClient.post<any>(`${environment.MAIN_API_URL}/register`, registerData);
    keycloakResponseObservable.subscribe(_ => {
    });

    return keycloakResponseObservable;
  }

}
