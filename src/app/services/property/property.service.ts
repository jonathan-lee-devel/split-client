import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {PropertyDto} from "../../interfaces/dtos/PropertyDto";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class PropertyService {

  constructor(private httpClient: HttpClient) {
  }

  getProperties(): Observable<PropertyDto[]> {
    return this.httpClient.get<PropertyDto[]>(`${environment.MAIN_API_URL}/property`);
  }
  
  getPropertyById(propertyId: string): Observable<PropertyDto> {
    return this.httpClient.get<PropertyDto>(`${environment.MAIN_API_URL}/property/${propertyId}`);
  }

  createProperty(property: PropertyDto): Observable<PropertyDto> {
    return this.httpClient.post<PropertyDto>(`${environment.MAIN_API_URL}/property`, property);
  }

}
