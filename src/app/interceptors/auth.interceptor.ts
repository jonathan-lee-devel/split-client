import {Injectable} from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpStatusCode
} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {TokenService} from "../services/auth/token.service";
import {catchError} from "rxjs/operators";
import {Router} from "@angular/router";
import {AuthService} from "../services/auth/auth.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private tokenService: TokenService, private authService: AuthService, private router: Router) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const accessToken = this.tokenService.getAccessToken();

    // TODO implement refresh upon 401
    if (accessToken) {
      request = request.clone({
        headers: request.headers
          .set("Authorization", `Bearer ${accessToken}`)
      });
    }

    // 401 errors must be handled by the interceptor
    return next.handle(request)
      .pipe(
        catchError((error) => {
          if (error instanceof HttpErrorResponse && error.status == HttpStatusCode.Unauthorized) {
            const refreshToken = this.tokenService.getRefreshToken();
            if (refreshToken != null) {
              this.authService.refreshToken({refresh_token: refreshToken}).subscribe(response => {
                if (!response.access_token) {
                  this.router.navigate(["/login"], {queryParams: {returnUrl: request.url}}).then(r => {
                  });
                }
              });
            }
            return of(error as any);
          }
          throw error;
        })
      );
  }
}
