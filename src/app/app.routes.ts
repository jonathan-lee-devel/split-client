import {Routes} from "@angular/router";
import {LoginComponent} from "./components/pages/login/login.component";
import {AboutComponent} from "./components/pages/about/about.component";
import {AuthGuard} from "./guards/auth.guard";
import {PropertyComponent} from "./components/pages/property/property.component";
import {ExpenseComponent} from "./components/pages/expense/expense.component";
import {HomeComponent} from "./components/pages/home/home.component";
import {PageNotFoundComponent} from "./components/pages/page-not-found/page-not-found.component";
import {ViewPropertyComponent} from "./components/pages/property/view-property/view-property.component";
import {ViewExpenseComponent} from "./components/pages/expense/view-expense/view-expense.component";
import {RegisterComponent} from "./components/pages/register/register.component";

export const appRoutes: Routes = [
  {path: "", component: HomeComponent},
  {path: "home", component: HomeComponent},
  {path: "login", component: LoginComponent},
  {path: "register", component: RegisterComponent},
  {path: "about", component: AboutComponent},
  {path: "property", canActivate: [AuthGuard], component: PropertyComponent},
  {path: "property/view", canActivate: [AuthGuard], component: ViewPropertyComponent},
  {path: "property/view/:propertyId", canActivate: [AuthGuard], component: ViewPropertyComponent},
  {path: "expense", canActivate: [AuthGuard], component: ExpenseComponent},
  {path: "expense/view", canActivate: [AuthGuard], component: ViewExpenseComponent},
  {path: "**", component: PageNotFoundComponent}
]
