import {Component, OnInit} from '@angular/core';
import {PropertyService} from "../../../services/property/property.service";

@Component({
  selector: 'app-property',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.css']
})
export class PropertyComponent implements OnInit {
  name: string = "";
  address: string = "";

  constructor(private propertyService: PropertyService) {
  }

  ngOnInit(): void {
  }

  doNewProperty(): void {
    const property = {name: this.name, address: this.address};

    this.propertyService.createProperty(property).subscribe(response => {
    });
  }

}
