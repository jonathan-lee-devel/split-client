import {Component, OnInit} from '@angular/core';
import {PropertyDto} from "../../../../interfaces/dtos/PropertyDto";
import {PropertyService} from "../../../../services/property/property.service";
import {ActivatedRoute, Router} from "@angular/router";
import {HttpStatusCode} from "@angular/common/http";
import {AuthService} from "../../../../services/auth/auth.service";

@Component({
  selector: 'app-view-property',
  templateUrl: './view-property.component.html',
  styleUrls: ['./view-property.component.css']
})
export class ViewPropertyComponent implements OnInit {

  properties: PropertyDto[] | undefined;
  private propertyId: string = "";

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private authService: AuthService, private propertyService: PropertyService) {
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.propertyId = params["propertyId"];
    })

    if (this.propertyId !== undefined) {
      this.propertyService.getPropertyById(this.propertyId).subscribe((response => {
      }), error => {
        if (error.status == HttpStatusCode.NotFound) {
          this.router.navigate(["/pageNotFound"]).then(r => {
          });
        }
      });
    } else {
      this.propertyService.getProperties().subscribe(properties => {
        this.properties = properties;
      });
    }
  }

}
