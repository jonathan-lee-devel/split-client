import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../../services/auth/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  firstname: string | undefined;
  lastname: string | undefined;
  username: string | undefined;
  password: string | undefined;
  confirmPassword: string | undefined;

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit(): void {
  }

  doRegister(): void {
    const registerData = {
      firstname: this.firstname!!,
      lastname: this.lastname!!,
      email: this.username!!,
      username: this.username!!,
      password: this.password!!,
      confirm_password: this.confirmPassword!!
    };

    this.authService.register(registerData);

    this.router.navigate(["/login"]).then(_ => false);
  }

}
