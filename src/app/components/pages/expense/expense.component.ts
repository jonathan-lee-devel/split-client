import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-expense',
  templateUrl: './expense.component.html',
  styleUrls: ['./expense.component.css']
})
export class ExpenseComponent implements OnInit {
  amount: number = 0.0;
  frequency: string = "ONCE";
  isActive: boolean = false;
  startDate: number = Date.now();

  constructor() {
  }

  ngOnInit(): void {
  }

  doNewExpense(): void {
  }

}
