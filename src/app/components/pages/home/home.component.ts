import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../../services/auth/auth.service";
import {PropertyDto} from "../../../interfaces/dtos/PropertyDto";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  isLoggedIn: boolean = false;
  properties: PropertyDto[] | undefined;

  constructor(private authService: AuthService) {
    this.authService.getIsLoggedIn().subscribe(isLoggedIn => {
        this.isLoggedIn = isLoggedIn
      }
    );
  }

  ngOnInit(): void {
    this.authService.getIsLoggedIn().subscribe(isLoggedIn => {
        this.isLoggedIn = isLoggedIn
      }
    );
    this.properties = [
      {name: "Example Property", address: "Example Street 123"},
      {name: "Another Property", address: "Another Street 321"}
    ]
  }

}
