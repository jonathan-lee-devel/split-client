export interface RegisterDto {
  firstname: string;
  lastname: string;
  email: string;
  username: string;
  password: string;
  confirm_password: string;
}
