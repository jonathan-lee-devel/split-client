export interface PropertyDto {
  name: string;
  address: string;
}
