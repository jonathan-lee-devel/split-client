import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {RouterModule} from "@angular/router";
import {HeaderComponent} from './components/header/header.component';
import {FooterComponent} from './components/footer/footer.component';
import {AboutComponent} from './components/pages/about/about.component';
import {LoginComponent} from './components/pages/login/login.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {PropertyComponent} from './components/pages/property/property.component';
import {ExpenseComponent} from './components/pages/expense/expense.component';
import {appRoutes} from "./app.routes";
import {FormsModule} from "@angular/forms";
import {HomeComponent} from './components/pages/home/home.component';
import {PageNotFoundComponent} from './components/pages/page-not-found/page-not-found.component';
import {ViewExpenseComponent} from './components/pages/expense/view-expense/view-expense.component';
import {ViewPropertyComponent} from './components/pages/property/view-property/view-property.component';
import {AuthInterceptor} from "./interceptors/auth.interceptor";
import { RegisterComponent } from './components/pages/register/register.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    AboutComponent,
    LoginComponent,
    PropertyComponent,
    ExpenseComponent,
    HomeComponent,
    PageNotFoundComponent,
    ViewExpenseComponent,
    ViewPropertyComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    FormsModule
  ],
  exports: [
    RouterModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
