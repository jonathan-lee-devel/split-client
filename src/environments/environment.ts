export const environment = {
  production: false,
  AUTH_API_URL: "http://localhost:8083/auth/realms/split",
  AUTH_CLIENT_ID: "split-api",
  AUTH_LOGIN_GRANT_TYPE: "password",
  AUTH_REFRESH_GRANT_TYPE: "refresh_token",
  MAIN_API_URL: "http://localhost:8080"
};
